# BirdSite

A browser extension for cross-posting Mastodon toots to Twitter.

## Features

- Cross-post a toot to Twitter right from the Mastodon web app.
- Attached images are also cross-posted.
- Messages longer than a tweet are truncated, and a link to the original toot is added.

![Screenshot of BirdSite UI: an 'Also post on the bird site' checkbox is added to Mastodon's compose form.](assets/chrome-web-store-banner.png)

## Usage

1. Install the extension for [Firefox](https://addons.mozilla.org/fr/firefox/addon/birdsite/) or [Chrome](https://chrome.google.com/webstore/detail/birdsite/lfmfhaopllgidldpmifdcjdckdggdjaj);
2. Open the web page of your favorite Mastodon instance;
3. Type your toot;
4. Click the “Also send to the bird site” button on the toot compose form;
5. Toot!

If you are not authenticated on Twitter yet, a pop-up will open to authenticate you,
and to ask for permission to post the tweet in your name. You only have to do this once.

Afterwards, toots sent while the checkbox is ticked will also be posted on your Twitter account.

## Alternatives

- Prefer cross-posting from Twitter (or TweetDeck) to Mastodon? _Have a look at the great [Tooter](https://github.com/ineffyble/tooter/) browser extension._
- Rather have all your toots and tweets cross-posted automatically? _Try the awesome [Mastodon Twitter Poster](https://crossposter.masto.donte.com.br/), and setup automatic mirroring between your microblogging accounts._
- Using native mobile apps? _The [Twidere](https://github.com/TwidereProject/Twidere-Android) app can cross-post to both Mastodon and Twitter._

## How to build

1. Clone the repository;
2. `make all`

## License

This extension is licensed under the terms of the GPL v3 license. (see LICENSE.md)
